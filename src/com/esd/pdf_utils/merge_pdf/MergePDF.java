package com.esd.pdf_utils.merge_pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class MergePDF {

	public static void main(String[] args) {

		String folderName = args[0]; // "C01175-PSVI0051249";
		String outputFolder = "C:\\ESD_DOCUMENTS\\Output";
		String directoryPath = outputFolder + "\\" + folderName;
		File dir = new File(directoryPath);
		String[] children = dir.list();

		try {
			List<InputStream> list = new ArrayList<InputStream>();
			OutputStream outputStream = new FileOutputStream(new File(outputFolder + "\\" + folderName + ".pdf"));

			if (children.length > 0) {
				for (String fileName : children) {
					InputStream inputStreamFile = new FileInputStream(new File(directoryPath + "\\" + fileName));
					list.add(inputStreamFile);
				}
			}

			mergePDFFiles(list, outputStream);
			System.out.println(folderName+".pdf merged.");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void mergePDFFiles(List<InputStream> list, OutputStream outputStream) {
		
		try {
			
			Document document = new Document();
			PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
			document.open();
			PdfContentByte pdfContentByte = pdfWriter.getDirectContent();

			for (InputStream inputStream : list) {
				
				PdfReader pdfReader = new PdfReader(inputStream);
				
				for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
					document.newPage();
					PdfImportedPage page = pdfWriter.getImportedPage(pdfReader, i);
					pdfContentByte.addTemplate(page, 0, 0);
				}
			}

			outputStream.flush();
			document.close();
			outputStream.close();

		} catch (DocumentException ex) {
			System.out.println("DocumentException >>> " + ex.getMessage());
		} catch (IOException ex) {
			System.out.println("IOException >>> " + ex.getMessage());
		}
	}

}
